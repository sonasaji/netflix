import React, { useEffect, useState } from 'react'
import axios from '../../axios'
import {imageUrl} from '../../constants/constants'
import './Banner.css'

function Banner(props) {
  const [movie, setMovie] = useState()
  const [num, setNum] = useState(0);

  const randomNumberInRange = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  useEffect(() => {
    setNum(randomNumberInRange(0, 19));
  }, [])

  useEffect(() => {
    axios.get(props.url)
      .then((response)=>{
        // console.log("res",response.data.results[0])
        setMovie(response.data.results[num])
      })
  }, [num])

  
  return (

    <div className='banner'
          style={{backgroundImage:`url(${movie ? imageUrl+movie.backdrop_path:""})`}}>
      <div className='content' >
        <h1 className='title'>
          { movie ? movie.title : "" }
        </h1>
        <div className='banner_buttons' >
          <button className='button' >Play</button>
          <button className='button' >My list</button>
        </div>
        <h1 className='description'>
          { movie ? movie.overview : "" }
        </h1>
      </div>
      <div className="fade_bottom"></div>
    </div>
  )
}

export default Banner