import React,{useState} from 'react'
// import { Container } from 'react-bootstrap'
import axios from 'axios'
import './SignIn.css';


const SignIn = () => {
    const [email,setEmail] = useState("")
    const [password,setPassword] = useState("")
    function LogIn(){
      console.log("helllooo")
      axios.post('http://localhost:3000/users/sign_in',
      {
        user: {
          email : email,
          password :password,
        }
      }
      ).then(result => {
        console.log(result)
        console.log(result.headers.authorization)
        window.alert("Email : "+ result.data.user.email)
        
          
      })
      .catch(error => {
          window.alert(error.response.data.error)
          console.log(error)
      })

  }


  return (
    
    <section className="background-radial-gradient overflow-hidden">
      <div className="container px-4 py-5 px-md-5 text-center text-lg-start my-5">
        <div className="row gx-lg-5 align-items-center mb-5">
          <div className="col-lg-6 mb-5 mb-lg-0 login-col">
            <h1 className="my-5 display-5 fw-bold ls-tight login-head">
              The best offer <br />
              <span className="offer-data">for your business</span>
            </h1>
            <p className="mb-4 opacity-70 offer-desc">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit.
              Temporibus, expedita iusto veniam atque, magni tempora mollitia
              dolorum consequatur nulla, neque debitis eos reprehenderit quasi
              ab ipsum nisi dolorem modi. Quos?
            </p>
          </div>

          <div className="col-lg-6 mb-5 mb-lg-0 position-relative">
            <div id="radius-shape-1" className="position-absolute rounded-circle shadow-5-strong"></div>
            <div id="radius-shape-2" className="position-absolute shadow-5-strong"></div>

            <div className="card bg-glass">
              <div className="card-body px-4 py-5 px-md-5">
                 
                  {/* <!-- Email input --> */}
                  <div className="form-outline mb-4">
                    <label className="form-label" htmlFor="form3Example3">Email address</label>
                    <input type="email" id="form3Example3" placeholder="Eg:abc@email.com" className="form-control" value={email} onChange={(e) => setEmail(e.target.value)} />
                  </div>

                  {/* <!-- Password input --> */}
                  <div className="form-outline mb-4">
                    <label className="form-label" htmlFor="form3Example4">Password</label>
                    <input type="password" id="form3Example4" placeholder="Password must contain 6 characters" className="form-control" value={password} onChange={(e) => setPassword(e.target.value)} /> 
                  </div>

                  {/* <!-- Checkbox --> */}
                  <div className="form-check d-flex justify-content-start mb-4">
                    <input className="form-check-input me-2" type="checkbox" value="" id="form2Example33" checked />
                    <label className="form-check-label" htmlFor="form2Example33">
                      Remember Me
                    </label>
                  </div>

                  {/* <!-- Submit button --> */}
                  <div className='submit_button text-center'>
                    <button onClick={LogIn} className="w-50 btn btn-primary btn-block mb-4">
                      Login
                    </button>
                  </div>
                {/* <!-- Register buttons --> */}
                <div className="text-center">
                  <p>or login with:</p>
                  <button type="button" className="btn btn-link btn-floating mx-1">
                    <i className="fab fa-facebook-f"></i>
                  </button>

                  <button type="button" className="btn btn-link btn-floating mx-1">
                    <i className="fab fa-google"></i>
                  </button>

                  <button type="button" className="btn btn-link btn-floating mx-1">
                    <i className="fab fa-twitter"></i>
                  </button>

                  <button type="button" className="btn btn-link btn-floating mx-1">
                    <i className="fab fa-github"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default SignIn