import React,{useState, useEffect} from 'react'
import axios from '../../axios'
import {API_KEY, imageUrl} from '../../constants/constants'
import YouTube from 'react-youtube';
import './RowPost.css'

function RowPost(props) {
  const [movies, setMovies]= useState([])
  const [urlId, seturlId] = useState('')
  const opts = {
    height: '390',
    width: '100%',
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 1,
    },
  };

  const handleMovieTrailer=(id)=>{
    axios.get(`/movie/${id}/videos?api_key=${API_KEY}&language=en-US`)
    .then((response)=>{
      console.log("res",response.data.results)
      if (response.data.results.length !==0){
        seturlId(response.data.results[0])
      }else{
        console.log("Trailer not available")
      } 
    })
  }

  useEffect(() => {
    axios.get(props.url)
    .then((response)=>{
      console.log("result",response.data.results)
      setMovies(response.data.results)
    }).catch(err=> {
      console.log("Network error")
    })
  }, [])
  console.log("mov",movies)
  return (
    <div className='row mt-3'>
      <h4>{props.title}</h4>
      <div className='posters'>
        {movies.map((obj, idx) => (
          <img onClick = {()=> handleMovieTrailer(obj.id)} className={props.isSmall? 'smallPoster':'poster'} alt='poster' src={`${imageUrl+obj.backdrop_path}`} />
        ))
          
        }
        
        
      </div>
      { urlId && <YouTube videoId={urlId.key} opts={opts}/>}
    </div>
  )
}

export default RowPost