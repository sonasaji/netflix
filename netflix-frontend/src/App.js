import NavBar from './Components/NavBar/NavBar'
import './App.css'
import Banner from './Components/Banner/Banner'
import RowPost from './Components/RowPost/RowPost'
import { Component } from 'react';
// import './Components/SignUp/SignUp.css';
// import SignUp from './Components/SignUp/SignUp';
// import './Components/SignIn/SignIn.css';
import {originals, action, movieBanner} from './urls'
// import SignIn from './Components/SignIn/SignIn';

class App extends Component {


  render() {
    return (
      <div className="App">
        <>
        <NavBar/>
        <Banner url={movieBanner} />
        <RowPost url={originals} title='Netflix Originals'/>
        <RowPost url={action} title='Action' isSmall/>
          {/* <SignUp /> */}
          {/* <SignIn /> */}
        </>
      </div>
    );
  } 
}

export default App;
